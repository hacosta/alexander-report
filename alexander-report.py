#!/usr/bin/python2

import argparse
import ConfigParser
import datetime
import io
import jinja2
import jira.client
import os
import os.path
import subprocess
import tempfile

DEFAULTS = """
[jira]
server = https://eng-gazzang.atlassian.net
user =
password =
"""

CONF = None
JIRA_CTX = None


def get_opened_issues(days_ago=14):
    since = datetime.datetime.now() - datetime.timedelta(days=days_ago)
    return JIRA_CTX.search_issues("assignee='%s' AND reporter = '%s' AND created >= '%s'" % (CONF.user, CONF.user, since.strftime('%Y/%m/%d')))


def get_closed_issues(days_ago=14):
    since = datetime.datetime.now() - datetime.timedelta(days=days_ago)
    qs = "assignee='%s' AND created >= '%s' AND status IN ('Done')" % (CONF.user, since.strftime('%Y/%m/%d'))
    return JIRA_CTX.search_issues(qs)


def get_non_story_pointed_issues():
    # customfield_10004 == storypoints
    qs = "assignee='%s' AND cf[10004] is empty" % CONF.user
    return JIRA_CTX.search_issues(qs)


def create_template():
    return jinja2.Template('''
Have you created new stories that track any new work that came along since last week?
{% for story in new_stories -%}
{{story.key}} - {{story.fields.summary}}
{% else -%}
Nope
{% endfor %}


Have you deleted or closed out any stories that are no longer active
{% for story in closed_stories -%}
{{story.key}} - {{story.fields.summary}}
{% else -%}
Nope
{% endfor %}

Have you story-pointed all of your stories?
{% for story in non_story_pointed_stories -%}
{{story.key}} - {{story.fields.summary}}
{% else -%}
Yes
{% endfor %}

Have you broken down your stories to be smaller sub-stories with new knowledge since last week
Are all your stories up-2-date with regard to status (todo, inprogress etc)
GO CHECK THIS
''').render(new_stories=get_opened_issues(), closed_stories=get_closed_issues(), non_story_pointed_stories=get_non_story_pointed_issues())


def get_editor():
    return os.environ.get('JIRA_EDITOR') or os.environ.get('EDITOR') or 'vi'


def parse_args():

    conf_parser = argparse.ArgumentParser(add_help=False)
    conf_parser.add_argument('--conf-file', '-c', help='Specify the config file')
    args, remaining = conf_parser.parse_known_args()

    config = ConfigParser.RawConfigParser()
    config.readfp(io.BytesIO(DEFAULTS))
    if args.conf_file:
        config.read([args.conf_file])
    else:
        config.read(['/etc/jira.conf', os.path.expanduser('~/.jira.conf'), 'jira.conf'])

    opts = dict(config.items('jira'))
    parser = argparse.ArgumentParser(parents=[conf_parser])
    parser.set_defaults(**opts)

    parser.add_argument('--server', '-s', help='jira server')
    parser.add_argument('--user', '-u', help='jira user')
    parser.add_argument('--password', '-p', help='jira password')
    args = parser.parse_args()

    args.server = args.server or os.environ.get('JIRA_SERVER') or opts['server']
    args.user = args.user or os.environ.get('JIRA_USER')
    args.password = args.password or os.environ.get('JIRA_PASSWORD')

    if not args.user:
        parser.error('Missing required argument: user')

    if not args.password:
        parser.error('Missing required argument: password')

    return args


def main():
    global CONF
    global JIRA_CTX

    CONF = parse_args()
    JIRA_CTX = jira.client.JIRA(basic_auth=(CONF.user, CONF.password), options={'server': CONF.server})
    template = create_template()
    tf = tempfile.NamedTemporaryFile()
    tf.write(template)
    subprocess.call([get_editor(), tf.name])


if __name__ == '__main__':
    main()
